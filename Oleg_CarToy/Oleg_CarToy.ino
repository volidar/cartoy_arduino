void setup() {
  Serial.begin(9600);
  for(int i = 2; i < 11; i++) pinMode(i, OUTPUT);
  //startTest();
  //reset();
}

void loop() {
  int speed = 255;
  int wait = 3000;
  //reset();
  setSpeed(0, HIGH, LOW, speed);
  setSpeed(1, HIGH, LOW, speed);
  setSpeed(2, HIGH, LOW, speed);
  delay(wait);
  setSpeed(0, LOW, HIGH, speed);
  setSpeed(1, LOW, HIGH, speed);
  setSpeed(2, LOW, HIGH, speed);
  delay(wait);
}

void startTest() {
  for(int motor = 0; motor < 3; motor++) {
    for(int speed = 1; speed < 16; speed++) setSpeedAndWait(motor, true, speed);
    for(int speed = 14; speed >= 0; speed--) setSpeedAndWait(motor, true, speed);
    for(int speed = 1; speed < 16; speed++) setSpeedAndWait(motor, false, speed);
    for(int speed = 14; speed >= 0; speed--) setSpeedAndWait(motor, false, speed);
  }
}

void reset() {
  setSpeed(0, HIGH, LOW, 0);
  setSpeed(1, HIGH, LOW, 0);
  setSpeed(2, HIGH, LOW, 0);
}

void setSpeed(int motor, int inA, int inB, int speed) {
  Serial.println(String("-> m") + motor + " a" + inA + " b" + inB + " ^" + speed);
  digitalWrite(2 + motor * 3, inA);
  analogWrite(2 + motor * 3 + 1, speed);
  digitalWrite(2 + motor * 3 + 2, inB);
}

void setSpeedAndWait(int motor, boolean direction, int speed) {
  int speedFF = speed * 17;
  if(direction) setSpeed(motor, HIGH, LOW, speedFF);
  else setSpeed(motor, LOW, HIGH, speedFF);
  delay(1000);
}

//
//boolean waitForCommandStart() {
//  while(Serial.available() >= 3) { //! Cmd Pwr
//    if(Serial.read() == '!') return true;
//  }
//  return false;
//}
//
//void reset() {
//  motor.run(RELEASE);
//  motor.setSpeed(0);
//  yellowAlarmTime = 0L;
//  redAlarmTime = 0L;
//  analogWrite(yellowPin, 0L);
//  analogWrite(redPin, 0L);
//  setLight(black);
//}
//
//void execute(char command, int power) {
//  //B break motor
//  //F forward speed
//  //Y yellow alarm
//  //R red alarm
//  //N stop alarm
//  //L light
//  switch(command) {
//    case 'B':
//      setMotor(motor, RELEASE, 0);
//      break;
//    case 'F':
//      setMotor(motor, FORWARD, power);
//      break;
//    case 'Y':
//      if(power == 0) {
//        yellowAlarmTime = 0L;
//        analogWrite(yellowPin, 0L);
//      } else {
//        if(yellowAlarmTime == 0L) {
//          yellowAlarmTime = millis();
//        }
//      }
//      break;
//    case 'R':
//      if(power == 0) {
//        redAlarmTime = 0L;
//        analogWrite(redPin, 0L);
//      } else {
//        if(redAlarmTime == 0L) {
//          redAlarmTime = millis();
//        }
//      }
//      break;
//    case 'N':
//      yellowAlarmTime = 0L;
//      analogWrite(yellowPin, 0L);
//      redAlarmTime = 0L;
//      analogWrite(redPin, 0L);
//      break;
//    case 'L':
//      setLight(light.Color(power, power, power));
//      break;
//  }
//}
//
//void setMotor(AF_DCMotor &motor, int mode, int power) {
//  motor.run(mode);
//  motor.setSpeed(power);
//}
//void alarm(int pin, long startTime) {
//  if(startTime != 0L) {
//    long now = millis();
//    int value = (int)((sin(3.14159 * 2.0 * ((now - startTime) % alarmPeriod) / alarmPeriod) + 1.0) / 2.0 * 255.0);
//    analogWrite(pin, value);
//  }
//}
//void setLight(COLOR color) {
//  for(int i = 0; i < 7; i++) light.setPixelColor(i, color);
//  light.show();
//}
//

